from flask import *
from pingan import *
import os

app = Flask(__name__)
app.config['SECRET_KEY'] = os.urandom(24)
w = worker(60 * 30, reporting)

@app.route('/')
def login():
    if 'user' in session and 'pwd' in session:
        return redirect('/status')
    else:
        return render_template('index.html')

@app.route('/status', methods=['POST', 'GET'])
def status():
    user = ''
    pwd = ''
    if 'user' in session and 'pwd' in session:
        user = session['user']
        pwd = session['pwd']
    elif 'user' in request.form and 'pwd' in request.form:
        user = request.form['user']
        pwd = request.form['pwd']
        session['user'] = user
        session['pwd'] = pwd
    w.put((user, pwd))
    timeStr = '还没有'
    succStr = '还没有'
    if (user, pwd) in w.ret:
        result: Optional[Tuple[str, float]] = w.ret[(user, pwd)]
        if type(result) is tuple and len(result) == 2 and type(result[0]) is str and type(result[1]) is float:
            succStr = result[0]
            t = time.localtime(result[1])
            timeStr = '%s:%s' % (t.tm_hour, t.tm_min)
    return render_template('status.html', succStr = succStr, timeStr = timeStr, user = user)


def main():
    w.start()
    app.run(port=8888, debug = False, host='0.0.0.0')

main()