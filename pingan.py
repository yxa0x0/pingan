# -*- coding: utf-8 -*-
from base64 import decode
from time import mktime, struct_time
from typing import *
import requests, json, base64, hashlib
import time
from functools import *
import threading


class report:
    def __init__(self, usr: str, pwd: str):
        self.usr = usr  # 手机号
        self.pwd = pwd  # 密码
        # 定义一个session()的对象实体s来储存cookie
        self.s = requests.Session()
        # self.s.proxies = {'http': 'http://localhost:8888', 'https': 'http://localhost:8888'}
        # self.s.verify = False
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Linux; Android 10; MI 9 Build/QKQ1.190825.002; wv) AppleWebKit/537.36 (KHTML, '
                          'like Gecko) Version/4.0 Chrome/81.0.4044.62 Mobile Safari/537.36Ant-Android-WebView ',
            'Authorization': 'BASIC '
                             'NTgyYWFhZTU5N2Q1YjE2ZTU4NjhlZjVmOmRiMzU3YmRiNmYzYTBjNzJkYzJkOWM5MjkzMmFkMDYyZWRkZWE5ZjY='
        }
        self.interUrl = 'https://h5api.xiaoyuanjijiehao.com/api/staff/interface'

    # 模拟登录
    def login(self):
        usr1 = "{\"LoginModel\":1,\"Service\":\"ANT\",\"UserName\":\"%s\"}" % self.usr
        log_url = "https://auth.xiaoyuanjijiehao.com/oauth2/token"
        data = {
            'password': hashlib.md5(self.pwd.encode()).hexdigest(),
            'grant_type': 'password',
            'username': str(base64.b64encode(usr1.encode('utf-8')), 'utf-8'),
        }
        log_page = self.s.post(log_url, headers=self.headers, data=data).text
        # 获取access_token
        token = json.loads(log_page.strip())["access_token"]
        # 更新header
        self.s.headers.update({'AccessToken': 'ACKEY_' + token})
        return self

    # 报平安
    def save(self):
        re_url = "https://h5api.xiaoyuanjijiehao.com/api/staff/interface?="
        # 选择口号、体温、位置
        # data 本身即是json格式
        data = "{\"Router\":\"/api/studentsafetyreport/report\",\"Method\":\"POST\",\"Body\":\"{" \
               "\\\"ReportArea\\\":\\\"xx市\\\",\\\"ReportCode\\\":\\\"984b5551-xxxx-xxxx-xxxx-005056bc6061\\\"," \
               "\\\"UID\\\":\\\"\\\",\\\"Temperature\\\":\\\"1\\\",\\\"ReportAreaLat\\\":\\\"{" \
               "\\\\\\\"lng\\\\\\\":119.xxxxxxxxxxx,\\\\\\\"lat\\\\\\\":35.xxxxxxxxxxx," \
               "\\\\\\\"of\\\\\\\":\\\\\\\"inner\\\\\\\"}\\\",\\\"ReportAreaChoiceCode\\\":\\\"\\\"," \
               "\\\"ReportAreaChoiceName\\\":\\\"xx省xx市xx市\\\"}\"} "
        # 报平安请求
        re_page = self.s.post(re_url, headers=self.headers, data=data.encode('utf-8'))
        # 解析json 获取反馈
        decoded = json.loads(re_page.text.strip())
        if "FeedbackText" in decoded:
            feedback = '报平安成功&desp=' + decoded["FeedbackText"]
        else:
            feedback = '报平安失败&desp=未知错误，请及时排查'
        # Server酱通知
        #self.server(feedback)

    def put_json(self, data: str) -> Any :
        headers = {
            'AccessToken': self.s.headers['AccessToken'],
            'User-Agent': 'Mozilla/5.0 (Linux; Android 10; ONEPLUS A6000 Build/QKQ1.190716.003; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/84.0.4147.125 Mobile Safari/537.36Ant-Android-WebView',
            'Origin': 'https://h5api.xiaoyuanjijiehao.com',
            'Referer': 'https://h5api.xiaoyuanjijiehao.com/classbodyreport/index.html',
            # 'Sec-Fetch-Site': 'same-origin',
            # 'Sec-Fetch-Mode': 'cors',
            # 'Sec-Fetch-Dest': 'empty',
            # 'X-Requested-With': 'com.zjelite.antlinkercampus',
            'Content-Type': 'application/x-www-form-urlencoded',
        }
        #print(headers)
        resp = self.s.post(self.interUrl, headers = headers, data = data.encode('utf-8'))
        print("post_json: data=%s\n resp=%s\n" % (data, resp.text))
        assert(resp.status_code == 200)
        decoded = json.loads(resp.text.strip())
        assert(decoded['FeedbackCode'] == 0)
        return decoded['Data']

    def get_time(self):
        time = ['TemperatureUpMorn', 'TemperatureUpNoon', 'TemperatureUpNight', 'TemperatureOutSchool'] 
        data = '{"Router":"/api/system/getsystemparams","Method":"POST","Body":"{\\\"SGroup\\\":\\\"function\\\",\\\"SType\\\":\\\"TemperatureUp\\\",\\\"vName\\\":\\\"%s\\\"}"}'
        ret : list[str] = []
        for t in time:
            ret.append(self.put_json(data % t))
        return ret
    
    def time_report(self, ti: str) -> int:
        data = '{"Router":"/api/temperatureup/querynoup","Method":"POST","Body":"{\\\"ClassCode\\\":\\\"\\\",\\\"Times\\\":\\\"%s\\\"}"}' % ti
        datain = self.put_json(data)['dataIn']
        if datain == None:
            return -1
        code: str = datain[0]['IntelUserCode']
        assert(code != '' and code != None)
        data = '{"Router":"/api/temperatureup/queryuped","Method":"POST","Body":"{\\\"ClassCode\\\":\\\"\\\",\\\"Times\\\":\\\"%s\\\"}"}' % ti
        self.put_json(data)
        data = '{"Router":"/api/studentncpback/puttemperature","Method":"POST","Body":"{\\\"user\\\":\\\"%s\\\",\\\"temperature\\\":\\\"1\\\",\\\"reportArea\\\":\\\"校内\\\",\\\"memo\\\":\\\"\\\"}"}' % code
        self.put_json(data)
        return 0
    # 温度上报
    def temp(self):
        temp_url = "https://h5api.xiaoyuanjijiehao.com/api/staff/interface"
        data = "{\"Router\":\"/api/studentncpback/puttemperature\",\"Method\":\"POST\",\"Body\":\"{" \
               "\\\"user\\\":\\\"24a04c8e-xxxx-xxxx-xxxx-00163e08212a\\\",\\\"temperature\\\":\\\"1\\\"," \
               "\\\"reportArea\\\":\\\"xx省xx市xx市\\\",\\\"memo\\\":\\\"\\\"}\"} "
        # 报平安请求
        temp_page = self.s.post(temp_url, headers=self.headers, data=data.encode('utf-8'))
        # 解析json 获取反馈
        decoded = json.loads(temp_page.text.strip())
        if "FeedbackText" in decoded:
            feedback = '体温上报成功&desp=' + decoded["FeedbackText"]
        else:
            feedback = '体温上报失败&desp=未知错误，请及时排查'
        print(feedback)
        #self.server(feedback)
    
    def pingan(self):
        times = self.get_time()
        lt = time.time()
        for t in times:
            if self.time_report(t) != -1:
                print('%s: succ\n' % t)
                return t
            print('%s: fail\n' % t)
        return ''

# def Report(re: report):
#     times = re.get_time()
#     for t in times: 
#         res = re.time_report(t)
#         if res == 0:
#             print('%s: succ\n' % t)
#         else:
#             print('%s: unavilable\n' % t)



class worker:
    def __init__(self, sleepTime, func):
        self.set = set()
        self.uLock = threading.Lock()
        self.sleptime = sleepTime
        self.func = func
        self.ret: Any = None
        def f():
            while True:
                self.uLock.acquire()
                print('asdsa')
                print(self.set)
                self.ret = self.func(self.set)
                self.uLock.release()
                time.sleep(sleepTime)

        self.thread = threading.Thread(target = f)

    def put(self, item):
        self.uLock.acquire()
        self.set.add(item)
        self.uLock.release()
    
    def start(self):
        self.thread.start()
    
def reporting(users: Set[Tuple[str, str]]):
    rmLst = []
    retLst: Dict[Tuple[str, str], Tuple[str, float]] = dict()
    for u in users:
        res = ''
        try:
            print('start run (%s:%s)' % (u[0], u[1]))
            res = report(u[0], u[1]).login().pingan()
        except Exception as e:
            print(e)
            print('(%s:%s) fail' % (u[0], u[1]))
            rmLst.append(u)
        if res != '':
            retLst[u] = (res, time.time())
    for r in rmLst:
        users.remove(r)
    return retLst

